

 Site - AngularJS 1.1.5 NgAnimate.org Sample 

Code Preview 

                    index.html
                    1<!doctype html>

2<html ng-app>

3<head>

4  <meta charset="utf-8">

5  <title>Top Animation</title>

6  <script>document.write('<base href="' + document.location + '" />');</script>

7  <link rel="stylesheet" href="style.css">

8  <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/css/bootstrap-combined.min.css" rel="stylesheet">

9  <script src="http://code.angularjs.org/1.1.5/angular.js"></script>

10</head>

11<body ng-init="names=['Igor Minar', 'Brad Green', 'Dave Geddes', 'Naomi Black', 'Greg Weber', 'Dean Sofer', 'Wes Alvaro', 'John Scott', 'Daniel Nadasi'];">

12  <div class="well" style="margin-top: 30px; width: 200px; overflow: hidden;">

13    <form class="form-search"> 

14        <div class="input-append">

15          <input type="text" ng-model="search" class="search-query" style="width: 80px">

16          <button type="submit" class="btn">Search</button>

17        </div>

18        <ul class="nav nav-pills nav-stacked">

19          <li ng-animate="'animate'" ng-repeat="name in names | filter:search">

20            <a href="#"> {{name}} </a>

21          </li> 

22      </ul>

23    </form>

24  </div>

25</body>

26</html>





 Site - AngularJS 1.1.5 NgAnimate.org Sample 

Code Preview 

                    style.css
                    1.animate-enter, 

2.animate-leave

3{ 

4  -webkit-transition: 400ms cubic-bezier(0.250, 0.250, 0.750, 0.750) all;

5  -moz-transition: 400ms cubic-bezier(0.250, 0.250, 0.750, 0.750) all;

6  -ms-transition: 400ms cubic-bezier(0.250, 0.250, 0.750, 0.750) all;

7  -o-transition: 400ms cubic-bezier(0.250, 0.250, 0.750, 0.750) all;

8  transition: 400ms cubic-bezier(0.250, 0.250, 0.750, 0.750) all;

9  position: relative;

10  display: block;

11} 

12 

13.animate-enter.animate-enter-active, 

14.animate-leave {

15  opacity: 1;

16  top: 0;

17  height: 30px;

18}

19 

20.animate-leave.animate-leave-active,

21.animate-enter {

22  opacity: 0;

23  top: -50px;

24  height: 0px;

25}




                



                
