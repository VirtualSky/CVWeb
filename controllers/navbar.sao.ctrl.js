app.controller("navbarCtrl", function(){
    


    /*************************
              Attributs
    *************************/

    var vm = this;
    var scrollTo = 0;

    var contenerFirstList = document.getElementById("navbarItemContentList");
    var itemFirstList = document.getElementsByClassName("FirstItemList");
    var divEmptyHeight = 0;
    var fakescrollBarFirstList = document.getElementById("scrollBarFirtList");
    var scrollBarFirstList = document.getElementById("navbarItemContentList");


    /*************************
              METHODS
    *************************/
    vm.OnMenuClick = function(){
    	if(vm.showNavMenu == false){
    		vm.showNavMenu = true;
    		document.getElementById("menuButton").src="Asset/navbar/RingedConfig.png";
    	}else{
    		vm.showNavMenu = false;
    		document.getElementById("menuButton").src="Asset/navbar/RingedConfig_on.png";
            //setPosition("right");
    	}
    };
    vm.OnItemMenuClick = function(id,index){
        var x = document.getElementById(id).checked;
        if(x == false){
            console.log("OnItemMenuClick checked");
            var element = document.getElementById("navItemContent");
            element.style.display = "inline-block";
            var divEmptys = document.getElementsByClassName("divEmpty");
            divEmptyHeight = contenerFirstList.clientHeight/2 - 20;
            for (i = 0; i < divEmptys.length; i++) {
                divEmptys[i].style.height = divEmptyHeight+"px";
            }
            document.getElementById("navbarItemContentList").scrollTop = divEmptyHeight;
            
            //set attribut to fake contener
            var ulHeight = document.getElementById("navbarItemContentListContent").clientHeight;
            document.getElementById("scrollBarFakeContener").style.height = (divEmptyHeight*2)+(ulHeight)+"px";
            
            setPosition(index,element);
        }
    };
    vm.OnItemFirstListClick = function(id,rid,index){
        console.log(" click on firstList"+ id + document.getElementById(rid).checked);
        var x = document.getElementsByClassName("FirstItemList");
        if(document.getElementById(rid).checked == false){
           document.getElementById(rid).checked = true;
            for (i = 0; i < x.length; i++) {
                 x[i].style.backgroundColor = "#ffffff";
            }
            document.getElementById(id).style.background = "#eba601";
            /*here display active image*/
            document.getElementById("secondList").style.display = "inline-block";
        }else{
            document.getElementById(rid).checked = false;
            document.getElementById(id).style.background = "#ffffff";
            /*here hide active image*/
        }

        ListBehaviour(id,1,listIsChecked("firstListInput"),index);
    };
    vm.OnItemSecondListClick = function(id,rid){
        console.log(" click on secondList"+ id + document.getElementById(rid).checked);
        var x = document.getElementsByClassName("SecondItemList");
        if(document.getElementById(rid).checked == false){
           document.getElementById(rid).checked = true;
            for (i = 0; i < x.length; i++) {
                 x[i].style.backgroundColor = "#ffffff";
            }
            document.getElementById(id).style.background = "#eba601";
            document.getElementById("secondList").style.display = "inline-block";
        }else{
            document.getElementById(rid).checked = false;
            document.getElementById(id).style.background = "#ffffff";
        }
        ListBehaviour(id,2,listIsChecked("SecondListInput"),"");
        document.getElementById("windowsContainer").style.display = "block";
    }
    vm.OnNoButtonClick = function(){
        console.log("click on NO button");
        document.getElementById("windowsContainer").style.display="none";
        resetList("SecondListInput","SecondItemList",2);
    };
    fakescrollBarFirstList.onscroll = function(){
        scrollBarFirstList.scrollTop = fakescrollBarFirstList.scrollTop;
    };
    scrollBarFirstList.onscroll = function(){
        fakescrollBarFirstList.scrollTop = scrollBarFirstList.scrollTop;
    };

    /*************************
              TOOLS
    *************************/

    //TODO change position and scroll on list.
    setPosition = function (index,elem){
        if(elem == navItemContent){
            var menuListContener =  document.getElementById("navbarMenuList");
            var itemMenu = document.getElementsByClassName("itemMenuList"); 
            var target2 = document.getElementById("navItemContent");
            var toal = 0;
            if(index == 0){
                total = -(target2.clientHeight/2 + 10)+(itemMenu[0].clientHeight/2) +"px";
            }else{
                total = -(target2.clientHeight/2 + 10)+((itemMenu[0].clientHeight)*index)+(itemMenu[0].clientHeight/2) +"px";
            }
           target2.style.top = total;
        }
    };
    resetList = function(inputClass,liClass,index){
        var input = document.getElementsByClassName(inputClass);
        var li = document.getElementsByClassName(liClass);
        for (i = 0; i < input.length; i++) {
            input[i].checked == false;
            li[i].style.backgroundColor = "#ffffff";
        }
        console.log(input+"/"+ li);
        ListBehaviour("",index,false,"");
    };
    ListBehaviour = function(id,indexList,checked,index){
        if(checked == true){
            console.log("active");
            if (indexList == 1){
                document.getElementById("navbarArrowExtTop").style.visibility = "hidden";
                document.getElementById("navbarArrowExtBottom").style.visibility = "hidden";
                document.getElementById("arrowMiddle").src="Asset/navbar/ArrowMiddleActive2.png";
                document.getElementById("firstList").style.opacity = 0.6;
                vm.goToCenterScroll(id,indexList,index);
            }else{
                vm.goToCenter(id,indexList);
            }
            
        }else{
            console.log("NoActive");
            if (indexList == 1){
               document.getElementById("navbarArrowExtTop").style.visibility = "visible"
                document.getElementById("navbarArrowExtBottom").style.visibility = "visible";
                document.getElementById("arrowMiddle").src="Asset/navbar/ArrowMiddle2.png";
                document.getElementById("secondList").style.display = "none";
                document.getElementById("firstList").style.opacity = 0.9;
                document.getElementById("navbarItemContentList").scrollTop = divEmptyHeight;

            } else{
                document.getElementById("secondItemList").style.transform = "";
            }
        } 
    };
    listIsChecked = function(nameClass){
        var input = document.getElementsByClassName(nameClass);
        var temp = false;
        for (i = 0; i < input.length; i++) {
            if(input[i].checked){
                temp = true;
                break;
            }else{
                temp = false;
            }
        }
        return temp;
    };
    vm.goToCenter = function(id,index){
        if(index == 2){
            var list = document.getElementsByClassName("SecondItemList");
            var container =document.getElementById("ItemContentSecondList");
            var ul = document.getElementById("secondItemList");
        }
        
        var selectedItem = document.getElementById(id);
        console.log(selectedItem.clientTop, selectedItem.offsetTop);
        scrollTo = -(selectedItem.offsetTop) + container.clientHeight / 2 - selectedItem.clientHeight / 2;
        console.log(scrollTo);
        ul.style.transform = "translateY("+scrollTo+"px)";
    };
    vm.goToCenterScroll = function(id,indexList,index){
        elem = document.getElementById(id);
        fakescrollBarFirstList.scrollTop = (index * elem.clientHeight);
        scrollBarFirstList.scrollTop = (index * elem.clientHeight);
    };


   /*************************
              DATA
    *************************/
	vm.navbarMenu=[
        {
			Title: 'Skills',
            url: 'Asset/navbar/RingedConfig.png',
            urlActive: 'Asset/navbar/RingedConfig_on.png',
            id: 'skill'
        },
        {
            Title: 'Experience',
            url: 'Asset/navbar/RingedConfig.png',
            urlActive: 'Asset/navbar/RingedConfig_on.png',
            id: 'Experience'
        },
        {
           	Title: 'Education',
            url: 'Asset/navbar/RingedConfig.png',
            urlActive: 'Asset/navbar/RingedConfig_on.png',
            id: 'Education'
        },
        {
            Title: 'Projects',
            url: 'Asset/navbar/RingedConfig.png',
            urlActive: 'Asset/navbar/RingedConfig_on.png',
            id: 'projects'
        },
        {
            Title: 'Personal',
            url: 'Asset/navbar/RingedConfig.png',
            urlActive: 'Asset/navbar/RingedConfig_on.png',
            id: 'Personal'
        }
    ];
    vm.navbarItemList=[
        {
            name: 'Web',
            img: 'Asset/navbar/RingedConfig.png',
            id: 'web',
            idRadio: 'Rweb'
        },
        {
            name: 'Mobile',
            img: 'Asset/navbar/RingedConfig.png',
            id: 'mobile',
            idRadio:'Rmobile'
        },
        {
            name: 'Robotique',
            img: 'Asset/navbar/RingedConfig.png',
            id: 'robotique',
            idRadio: 'Rrobotique'
        },
        {
            name: 'IA',
            img: 'Asset/navbar/RingedConfig.png',
            id: 'ia',
            idRadio: 'Ria'
        },
        {
            name: 'Virtual Univers',
            img: 'Asset/navbar/RingedConfig.png',
            id: 'virtual',
            idRadio:'Rvirtual'
        },
        {
            name: 'manga',
            img: 'Asset/navbar/RingedConfig.png',
            id: 'manga',
            idRadio:'Rmanga'
        },
        {
            name: 'Life',
            img: 'Asset/navbar/RingedConfig.png',
            id: 'Life',
            idRadio:'Rlife'
        }
    ];
    vm.navbarItemSecondList=[
        {
            name: 'lol',
            img: 'Asset/navbar/RingedConfig.png',
            id: 'lol',
            idRadio: 'Rlol'
        },
        {
            name: 'Mobile',
            img: 'Asset/navbar/RingedConfig.png',
            id: 'mobile2',
            idRadio:'Rmobile2'
        },
        {
            name: 'Robotique',
            img: 'Asset/navbar/RingedConfig.png',
            id: 'robotique2',
            idRadio: 'Rrobotique2'
        },
        {
            name: 'IA',
            img: 'Asset/navbar/RingedConfig.png',
            id: 'ia2',
            idRadio: 'Ria2'
        },
        {
            name: 'Virtual Univers',
            img: 'Asset/navbar/RingedConfig.png',
            id: 'virtual2',
            idRadio:'Rvirtual2'
        },
        {
            name: 'manga',
            img: 'Asset/navbar/RingedConfig.png',
            id: 'manga2',
            idRadio:'Rmanga2'
        },
        {
            name: 'Life',
            img: 'Asset/navbar/RingedConfig.png',
            id: 'Life2',
            idRadio:'Rlife2'
        },
        {
            name: 'IA',
            img: 'Asset/navbar/RingedConfig.png',
            id: 'ia2',
            idRadio: 'Ria2'
        },
        {
            name: 'Virtual Univers',
            img: 'Asset/navbar/RingedConfig.png',
            id: 'virtual2',
            idRadio:'Rvirtual2'
        },
        {
            name: 'manga',
            img: 'Asset/navbar/RingedConfig.png',
            id: 'manga2',
            idRadio:'Rmanga2'
        },
        {
            name: 'Life',
            img: 'Asset/navbar/RingedConfig.png',
            id: 'Life2',
            idRadio:'Rlife2'
        }
    ];
});