var app = angular.module("app",[]);

app.controller("MainController", function(){
    var vm = this;
 	vm.tabsnav =[
		{
            tabTitle: 'Skills',
            url: 'skill.html',
            id: 'skill'
        },
        {
            tabTitle: 'Experience',
            url: 'Experience.html',
            id: 'Experience'
        },
        {
           	tabTitle: 'Education',
            url: 'Education.html',
            id: 'Education'
        },
        {
            tabTitle: 'Projects',
            url: 'projects.html',
            id: 'projects'
        },

        {
            tabTitle: 'Personal',
            url: 'Personal.html',
            id: 'Personal'
        }
	];
});