app.controller("healthBarCtrl", function () {
	var vm = this;
	vm.healthbar = "Asset/healthBar/greenBarStatuslarge.png";
	vm.name = "Hardy Thibaut";
	vm.currentLife = "9250";
	vm.maxLife = "18500";
	vm.level = "2";
	var i = 50;
	var img = null;
	vm.reset = function () {
		i = 50;
		document.getElementById("healthBarContainerBarStatus").style.width = "50%";
		document.getElementById("healthBarImgStatus").src = "Asset/healthBar/greenBarStatuslarge.png";
		document.getElementById("lifeCurrent").innerHTML = vm.maxLife;
		vm.currentLife = vm.maxLife;
	}
	vm.Life = function barCycle() {
		if (i-- <= 0) return;
		setTimeout(function () {
			document.getElementById("healthBarContainerBarStatus").style.width = i + "%";
			console.log(i);
			img = SetBarStatusImg(i, img);
			barCycle();
		}, 50);
	};

	vm.loopLife = function lifeCycle() {
		var percent = (vm.currentLife / vm.maxLife) * 100;
		console.log(vm.currentLife + ":" + percent);
		if (vm.currentLife < -1) return;
		setTimeout(function () {
			document.getElementById("healthBarContainerBarStatus").style.width = percent + "%";
			document.getElementById("lifeCurrent").innerHTML = vm.currentLife;
			console.log(vm.currentLife + ":" + percent);
			img = SetBarStatusImg(percent, img);
			vm.currentLife -= 500;
			lifeCycle();
		}, 50);
	};

	vm.setCurrentLife = function (value) {
		var percent = (value / vm.maxLife) * 100;
		document.getElementById("healthBarContainerBarStatus").style.width = percent + "%";
		document.getElementById("lifeCurrent").innerHTML = value;
		console.log(i);
		img = SetBarStatusImg(percent, img);
	};
	/*document.getElementById("healthBarContainerBarStatus").style.width = value+"%";*/
});

function SetBarStatusImg(i, img) {
	if (i < 20) {
		img = "Asset/healthBar/redBarStatuslarge.png";
	}
	else if (i <= 45 && i > 15) {
		img = "Asset/healthBar/orangeBarStatuslarge.png";
	}
	else {
		img = "Asset/healthBar/greenBarStatuslarge.png";
	}
	document.getElementById("healthBarImgStatus").src = img;
	return img;
}
